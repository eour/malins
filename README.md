# MALINS

[[_TOC_]]

## Technologie

### Casque Audio
- Sennheiser HD25 [:notebook:](https://fr-fr.sennheiser.com/casque-dj-on-ear-hd25) [:money_with_wings:](https://www.thomann.de/fr/sennheiser_hd_25.htm) :fire: *Léger et puissant. Pièces de rechange disponibles à la vente.*  
- Beyerdynamic DT-770 [:notebook:](https://europe.beyerdynamic.com/amfile/file/download/file_id/3878/product_id/2912/) [:money_with_wings:](https://www.thomann.de/fr/beyerdynamic_dt770_pro80_ohm.htm) :fire: *Confortable et précis. Pièces de rechange disponibles à la vente.*

### Montres
- Orient Bambino [:notebook:](https://www.lecalibre.com/orient-bambino/) [:money_with_wings:](https://ocarat.com/recherche?id_search=1&search=bambino&#marque-orient/-bambino/orderby-position/orderway-asc/n-42) :fire: *Calibre automatique japonais, rapport qualité-prix incroyable.*  
  ![orient bambino](/illustrations/Orient_Bambino.png)  
- Casio G-Shock GW-5610 [:notebook:](https://www.g-shock.eu/fr/montres/the-origin/gw-m5610u-1er/) [:money_with_wings:](https://ocarat.com/montre-g-shock-gw-m5610u-1er-casio-g-shock-77723.html) :fire: *Monstre solaire radiocontrolée. C’est le CHOC !*  

## Aventure

### Lampe frontale

- Nitecore HC-65 V2 [:notebook:](https://www.nitecore.fr/lampe-frontale-hc65-v2-1750lm-lg-908mm-nl1835hp-inclu-usb-c-c2x36444783) [:money_with_wings:](https://fr.aliexpress.com/wholesale?catId=0&initiative_id=AS_20220529054147&SearchText=nitecore+hc65+v2&spm=a2g0o.home.1000002.0) :fire: *Cellule d’accumulateurs 18650 très facile à trouver d'occasion.*  
  ![nitecore hc-65 v2](/illustrations/nitecore hc-65 v2.jpg)  

## Jeux

### Casses-têtes
- MeiLong 3×3 [:notebook:](https://www.thecubicle.com/collections/3x3-speed-cubes/products/mfjs-meilong-3x3) [:money_with_wings:](https://www.variantes.com/cubes-magiques-boutique/52633-cube-3x3-basic-stickerless-weilong-9999000007328.html) :fire: *Un produit très peu cher… qui a le [record du monde](https://www.worldcubeassociation.org/results/rankings/333/single) !*  

## Maison

### Soin de soi

Moser 1400 [:notebook:](https://www.moserpro.fr/index.php?tondeuse-coupe-1400-fr) [:money_with_wings:](https://www.laboutiqueducoiffeur.com/tondeuse-de-coupe-1400-professionnal.html) :fire: *Produit professionnel. Pièces de rechange disponibles à la vente. Filaire.*
